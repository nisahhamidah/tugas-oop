<?php
    require_once ('animal.php');
    require_once ('frog.php');
    require_once ('ape.php');


    $animal = new Animal ("shaun");
    echo "Name = ".$animal->name ."<br>";
    echo "Legs = ".$animal->legs . "<br>";
    echo "cold blooded = ".$animal-> cold_blooded . "<br>";
    echo "<br>";

    $frog = new frog ("Buduk");
    echo "Name = ".$frog->name ."<br>";
    echo "Legs = ".$frog->legs . "<br>";
    echo "cold blooded = ".$frog-> cold_blooded . "<br>";
    echo "Jump = ";
    $frog->jump();
    echo "<br><br>";

    $ape = new ape ("Kera Sakti");
    echo "Name = ".$ape->name ."<br>";
    echo "Legs = ".$ape->legs . "<br>";
    echo "cold blooded = ".$ape-> cold_blooded . "<br>";
    echo "Yell = ";
    $ape->yell();
?>